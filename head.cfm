<head>
	<title>Trundel</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,900,900italic,700italic,700,500italic,400italic,500,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
 	<link rel="stylesheet" href="css/libs/normalize.min.css">
    <link rel="stylesheet" href="css/libs/notifybar.css">
    <link rel="stylesheet" href="css/libs/croppie.css">
    <link rel="stylesheet" href="css/libs/animate.min.css">
 	<!--- Font Awesome --->
	<script src="https://use.fontawesome.com/797ed67cda.js"></script>
	<!--- Font Awesome --->
	<link rel='stylesheet' href='css/all.css'>
	<link rel='stylesheet' href='css/shared.css'>
    <script type="text/javascript" src="js/libs/jquery.min.js"></script>
    <script type="text/javascript" src="js/libs/croppie.js"></script>
    <script type="text/javascript" src="js/libs/notifybar.js"></script>
    <script type="text/javascript" src="js/libs/animatedModal.js"></script>
	<script type="text/javascript" src="js/shared.js"></script>
</head>
