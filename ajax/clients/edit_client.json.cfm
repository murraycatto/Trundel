<cfquery name="insert_client" datasource="#application.db#">
	UPDATE Clients
	SET 
	Name = <cfqueryparam cfsqltype="cf_sql_varchar" value="#form.Name#">,
	Image = <cfqueryparam value="#form.Image#">,
	ClientColorID = <cfqueryparam cfsqltype="cf_sql_integer"  value="#form.ClientColorID#">,
	ClientTypeID = <cfqueryparam cfsqltype="cf_sql_integer" value="#form.ClientTypeID#">
	WHERE ClientID = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.ClientID#">
</cfquery>
{"success":"1","message":"Client Updated!"}