<cfquery name="insert_client" datasource="#application.db#">
	INSERT INTO Clients
	(
		Name,
		Image,
		ClientTypeID,
		ClientColorID,
		UserCompanyID,
		UserID,
		DateAdded
	)
	Values(
		<cfqueryparam cfsqltype="cf_sql_varchar" value="#form.Name#">,
		<cfqueryparam cfsqltype="cf_sql_longvarchar" value="#form.Image#">,
		<cfqueryparam cfsqltype="cf_sql_integer" value="#form.ClientTypeID#">,
		<cfqueryparam cfsqltype="cf_sql_integer" value="#form.ClientColorID#">,
		<cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserCompanyID#">,
		<cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserID#">,
		<cfqueryparam cfsqltype="cf_sql_timestamp" value="#Now()#">
	)
</cfquery>
{"success":"1","message":"Client Added!"}