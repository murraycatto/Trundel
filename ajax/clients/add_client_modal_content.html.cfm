<section class="container" id="AddClientForm">
	<h1 class="fancy-header">Add Client</h1>
	<input type="text" placeholder="Client Name" class="modal-input" name="Name"/>
	<cfquery name="get_client_colors" datasource="#application.db#">
		SELECT * from lk_client_colors
	</cfquery>
	<div class="clear-30"></div>
	<cfoutput query="get_client_colors">
		<div class="client-color" style="background:#HexCode#" onclick="selectColor(this,'#ClientColorID#');"><span>#Name#</span></div>
	</cfoutput>
	<div class="clear-30"></div>
	<div class="cursor-pointer" onclick="$('#upload').click();">
		<i class="fa fa-picture-o modal-picture-upload pull-left" aria-hidden="true"> </i>
		<div class="modal-text pull-left">Choose a thumbnail</div>
	</div>
	<img src="" alt="" style="display:none" id="upload-thumbnail-img">
	<input id="upload" value="Choose a file" accept="image/*" type="file" style="display:none;"/>
	<div id="upload-thumbnail" style="display:none;"></div>
	<div class="clear-30"></div>
	<button class="upload-result" style="display:none;">Done Cropping</button>
	<input type="hidden" id="upload-thumbnail-result" name="Image" />
	<input type="hidden" name="ClientTypeID" value="1">
	<input type="hidden" name="ClientColorID" id="ClientColorID">
	<button class="btn-save" style="display:none;" onclick="dynamicAjaxSubmitForm('clients/save_client.json.cfm','','AddClientForm', '$(\'.close-animatedModal\').click();getClients();')">Save Client</button>
</section>