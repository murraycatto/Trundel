<cfparam name="url.ClientID" default="">
<cfquery name="get_client" datasource="#application.db#">
	SELECT * from Clients 
	where ClientID = <cfqueryparam cfsqltype="cf_sql_integer" value="#url.ClientID#">
</cfquery>
<cfoutput>
<section class="container" id="EditClientForm">
	<h1 class="fancy-header">Edit Client</h1>
	<input type="text" placeholder="Client Name" class="modal-input" name="Name" value="#get_client.Name#" />
	<cfquery name="get_client_colors" datasource="#application.db#">
		SELECT * from lk_client_colors
	</cfquery>
	<div class="clear-30"></div>
	<cfloop query="get_client_colors">
		<div class="client-color <cfif ClientColorID eq get_client.ClientColorID>active</cfif>" style="background:#HexCode#" onclick="selectColor(this,'#ClientColorID#');"><span>#Name#</span></div>
	</cfloop>
	<div class="clear-30"></div>
	<div class="cursor-pointer" onclick="$('##upload').click();">
		<i class="fa fa-picture-o modal-picture-upload pull-left" aria-hidden="true"> </i>
		<div class="modal-text pull-left">Choose a thumbnail</div>
	</div>
	<img src="#get_client.Image#" alt="" id="upload-thumbnail-img">
	<input id="upload" value="Choose a file" accept="image/*" type="file" style="display:none;"/>
	<div id="upload-thumbnail" style="display:none;"></div>
	<div class="clear-30"></div>
	<button class="upload-result" style="display:none;">Done Cropping</button>
	<input type="hidden" id="upload-thumbnail-result" name="Image" value="#get_client.Image#"/>
	<input type="hidden" name="ClientTypeID" value="1">
	<input type="hidden" name="ClientColorID" id="ClientColorID" value="#get_client.ClientColorID#">
	<button class="btn-save" onclick="dynamicAjaxSubmitForm('clients/edit_client.json.cfm?ClientID=#get_client.ClientID#','','EditClientForm', '$(\'.close-animatedModal\').click();getClients();')">Update Client</button>
	<button class="btn-delete" onclick="dynamicAjaxSubmitForm('clients\delete_client.json.cfm?ClientID=#get_client.ClientID#','','', '$(\'.close-animatedModal\').click();getClients();')">Delete Client</button>
</section>
</cfoutput>