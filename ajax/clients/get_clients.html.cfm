<cfoutput>
<cfquery name="get_clients" datasource="#application.db#">
	SELECT c.*, lcc.HexCode
	FROM Clients c
	Left join lk_client_colors lcc on c.ClientColorID = lcc.ClientColorID
	WHERE c.UserCompanyID = <cfqueryparam cfsqltype="cf_sql_integer" value="#session.UserCompanyID#">
</cfquery>
<cfloop query="get_clients">
	<figure class="white">
		<a href="details.html">
			<img src="#image#" alt=""/>
		</a>
	    <div id="wrapper-part-info" style="border-bottom: 3px solid #HexCode#;">
	    	<div class="client-name">
	    		#Name# <i class="fa fa-ellipsis-v" onclick='dynamicAjaxGet("clients/edit_client_modal_content.html.cfm?ClientID=#ClientID#","ModalContent","initClientThumbnailCropper();openModal();");'></i>
	    	</div>
		</div>
	</figure>	
</cfloop>
</cfoutput>