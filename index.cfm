<!DOCTYPE HTML>
<html>
<cfinclude template="head.cfm">
<body>
	<cfinclude template="header.cfm">
	<div id="wrapper-container">
		<div class="container object">
			<div id="main-container-image">
				<section class="item">
					<div class="add-item" onclick='dynamicAjaxGet("clients/add_client_modal_content.html.cfm","ModalContent","initClientThumbnailCropper();openModal();");'>
						+
					</div>
					<div id="ClientsHolder"></div>
				</section>
			</div>
		</div>
	</div>
	<cfinclude template="footer.cfm">
</body>
</html>
