$(document).ready(function() {
	$("#wrapper-header").delay(500).animate({opacity:'1',width:'100%'},500);
	$("#main-container-image").delay(1000).animate({opacity:'1'},500);
	if($("#ModalLink").length === 1){
		 $("#ModalLink").animatedModal();
	}
	if($("#ClientsHolder").length === 1){
		getClients();
	}
});
function openModal(){
	$("#ModalLink").click();
}
function getClients(){
	 dynamicAjaxGet("clients/get_clients.html.cfm",'ClientsHolder');
}
function selectColor(el,ClientColorID){
	$(".client-color").removeClass("active");
	$(el).addClass("active");
	$("#ClientColorID")['0'].value = ClientColorID;
}
function initClientThumbnailCropper(){
	var $uploadCrop;
	function readFile(input) {
			if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
				$('.upload-thumbnail').addClass('ready');
            	$uploadCrop.croppie('bind', {
            		url: e.target.result
            	}).then(function(){
            		console.log('jQuery bind complete');
            	});
            	$("#upload-thumbnail").slideDown();
            	$(".upload-result").slideDown();
            	$(".btn-save").slideUp();
            }
            reader.readAsDataURL(input.files[0]);
        }
	}

	$(function(){
		$uploadCrop = $('#upload-thumbnail').croppie({
			viewport: {
				width: 400,
				height: 400
			},
			enableExif: true
		});
		$('#upload').on('change', function () { readFile(this); });
		$('.upload-result').on('click', function (ev) {
			$uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (resp) {
				$("#upload-thumbnail-result")['0'].value = resp;
				$("#upload-thumbnail-img")['0'].src = resp;
				$("#upload-thumbnail-img").slideDown();
				$("#upload-thumbnail").slideUp();
            	$(".upload-result").slideUp();
            	$(".btn-save").slideDown();
			});
		});
	});
}

function dynamicAjaxGet(FileName,ReplaceHolder,callBacks, hasLoading){
	if(callBacks === undefined){
	    callBacks = '';
	}
	if(hasLoading === undefined){
	    hasLoading = false;
	}
	if(hasLoading){
		$("#"+ReplaceHolder)['0'].innerHTML = "<img src='images/loading.gif' id='loading'/>";
	}
	$.ajax({
		method: "Get",
		url: "ajax/"+FileName
	}) .done(function( data ) {
		if(hasLoading){
		    $( "#loading" ).fadeOut( 500, function() {
				$("#"+ReplaceHolder)['0'].innerHTML = data;
				eval(callBacks);
				eval($(data).find("script").text());
			});
		}else{
			$("#"+ReplaceHolder)['0'].innerHTML = data;
			eval(callBacks);
			eval($(data).find("script").text());
		}
	});
	return false;
}

function dynamicAjaxSubmitForm(FileName,ReplaceHolder,FormID, callBacks,failedCallback){
	if(callBacks === undefined){
	    callBacks = '';
	}
	if(failedCallback === undefined){
	    failedCallback = '';
	}
	var form_fields = {}; 
	$('#'+FormID+' input').each(function(i,el){
		form_fields[$(el)['0'].name] = $(el)['0'].value;
		console.log(el);
	});
	$('#'+FormID+' select').each(function(i,el){
		form_fields[$(el)['0'].name] = $(el)['0'].value;
	});
	$('#'+FormID+' textarea').each(function(i,el){
		form_fields[$(el)['0'].name] = $(el)['0'].value;
	});
	
	$.ajax({
		method: "Post",
		url: "ajax/"+FileName,
		data:form_fields
	}) .done(function( data ) {
		handleResponse(data,callBacks,failedCallback);
	});
}

function handleResponse(Response,callBacks,failedCallback){
	if(callBacks === undefined){
	    callBacks = '';
	}
	if(failedCallback === undefined){
	    failedCallback = '';
	}
 	try {
        response = JSON.parse(Response);
        success = response.success;
        message = response.message;
        if(success == "0"){
    		showMessage("<b>Error:</b> "+message,"error");
    		eval(failedCallback);
        }else{
    		showMessage("<b>Success:</b> "+message,"success");
    		console.log(callBacks);
    		eval(callBacks);
        }
    } catch(err) {
        showMessage("<b>Error:</b> An Unknown Error has occured.","error");
        console.log(err);
    }
}

function showMessage(msg,msg_class){
	$.notifyBar({
        cssClass: msg_class,
        html: msg
    });
}